<?php

namespace Clases;



/**
 * Clase de Habitacion
 * Objeto que contiene la informacion de una habitacion con su constructor para darle el valor a los datos
 */
class Habitacion {
    /**
     * @var int $m2 Metros cuadradros de la habitacion
     */
    protected $m2;
    /**
     * @var boolean $ventana Si dispone o no de ventana
     */
    protected $ventana;
    /**
     * @var string $tipo_habitacion Tipo de habitacion
     */
    protected $tipo_habitacion;
    /**
     * @var boolean $servicio_limpieza Si la habitacion dispone de servicio de limpieza
     */
    protected $servicio_limpieza;
    /**
     * @var boolean $internet Si la habitacion dispone de internet
     */
    protected $internet;
    /**
     * @var int $precio Precio de la habitacion
     */
    protected $precio;

   /**
     * @var string $descripcion Descripcion de la habitacion
     */
    protected $descripcion;

    /**
     * @param mixed $m2
     * @param mixed $ventana
     * @param mixed $tipo_habitacion
     * @param mixed $servicio_limpieza
     * @param mixed $internet
     * @param mixed $precio
     * @param mixed $descripcion
     */
    function __construct($m2, $ventana, $tipo_habitacion, $servicio_limpieza, $internet, $precio,$descripcion){
        $this->m2 = $m2;
        $this->ventana = $ventana;
        $this->tipo_habitacion = $tipo_habitacion;
        $this->servicio_limpieza = $servicio_limpieza;
        $this->internet = $internet;
        $this->precio = $precio;
        $this->descripcion= $descripcion;
    }

    
}