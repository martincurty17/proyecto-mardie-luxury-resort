<?php
namespace Clases;

class Registrado extends Usuario{
    
    /**
     * @param mixed $nombre
     * @param mixed $email
     * @param mixed $telf
     * @param mixed $direccion
     * @param mixed $password
     * @param mixed $rol
     */
    function __construct($nombre, $email, $telf, $direccion, $password, $rol){
        $this->nombre = $nombre;
        $this->email = $email;
        $this->telf = $telf;
        $this->direccion = $direccion;
        $this->password = $password;
        $this->rol = $rol;
    }


}


?>