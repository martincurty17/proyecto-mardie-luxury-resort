<?php

namespace Clases;


/**
 * Clase Reserva
 * Contiene la informacion de la reserva de una habitacion 
 */
class Reserva {
    /**
     * @var int $id_usuario Identificador del usuario que hace de la reserva
     */
    protected $id_usuario;
    /**
     * @var string $fecha_entrada Fecha de comienzo de la reserva
     */
    protected $fecha_entrada;
    /**
     * @var string $fecha_salida Fecha de finalizacion de la reserva
     */
    protected $fecha_salida;
    

    /**
     * @param mixed $id_usuario
     * @param mixed $fecha_entrada
     * @param mixed $fecha_salida
     */
    function __construct($id_usuario, $fecha_entrada, $fecha_salida){
        $this->id_usuario = $id_usuario;
        $this->fecha_entrada = $fecha_entrada;
        $this->fecha_salida = $fecha_salida;
    }

    
}