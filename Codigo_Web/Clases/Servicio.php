<?php

namespace Clases;


/**
 * Clase Servicio
 * Contiene la informacion de un servicio disponible
 */
class Servicio {
    /**
     * @var string $nombre_servicio Nombre del servicio
     */
    protected $nombre_servicio;
    /**
     * @var int $precio_servicio Precio del servicio
     */
    protected $precio_servicio;
    /**
     * @var string $descripcion Descripcion de que trata el servicio
     */
    protected $descripcion;
    /**
     * @var boolean $disponibilidad Determina si el servicio esta o no disponible
     */
    protected $disponibilidad;
    

    /**
     * @param mixed $nombre_servicio
     * @param mixed $precio_servicio
     * @param mixed $descripcion
     * @param mixed $disponibilidad
     */
    function __construct($nombre_servicio, $precio_servicio, $descripcion,$disponibilidad){
        $this->nombre_servicio = $nombre_servicio;
        $this->precio_servicio = $precio_servicio;
        $this->descripcion = $descripcion;
        $this->disponibilidad = $disponibilidad;
    }

    
}