<?php

namespace Clases;


/**
 *  Clase Usuario
 * Define la informacion de un usuario 
 */
class Usuario {
    /**
     * @var string $nombre Nombre del usuario
     */
    protected $nombre;
    /**
     * @var string $email Email del Usuario
     */
    protected $email;
    /**
     * @var int $telf Telefono del usuario
     */
    protected $telf;
    /**
     * @var string $direccion Direccion del usuario
     */
    protected $direccion;
    /**
     * @var string $password Contraseña del usuario 
     */
    protected $password;
    /**
     * @var int $rol Rol que ocupa el usuario
     */
    protected $rol;

    /**
     * @param mixed $nombre
     * @param mixed $email
     * @param mixed $telf
     * @param mixed $direccion
     * @param mixed $password
     * @param mixed $rol
     */
    function __construct($nombre, $email, $telf, $direccion, $password, $rol){
        $this->nombre = $nombre;
        $this->email = $email;
        $this->telf = $telf;
        $this->direccion = $direccion;
        $this->password = $password;
        $this->rol = $rol;
    }

    
}