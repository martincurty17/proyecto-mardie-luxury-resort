<?php

/**
 * Conexion a la base de datos con maximos privilegios
 */
namespace DB;

class Conexion{

    /**
     * @var String Define el nombre de la base de datos a la que se accede
     */
    private $nombreBBDD;
    /**
     * @var String Define el nombre de usuario con el que accedemos a la base de datos.
     */
    private $usuario;
    /**
     * @var String Contraseña del usuario con el que accedemos a la base de datos
     */
    private $clave;
    /**
     * @var String Direccion del servidor en el que se aloja la base de datos
     */
    private $servidor;
    /**
     * @var PDO 
     */
    public $PDO;


    /**
     * Contructor de la clase que nos permite crear una conexion a la base de datos asignada a nuestra configuracion
     * 
     */
    function __construct()
    {

        $datos = $this->leer_configuracion(__DIR__ . '/../config/configuracion.xml', __DIR__ . '/../config/configuracion.xsd');

        $this->nombreBBDD = $datos['nombre'];
        $this->servidor = $datos['servidor'];
        $this->usuario = $datos['usuario'];
        $this->clave = $datos['clave'];

        $this->PDO = $this->conectar();
    }

    /**
     * @return PDO Funcion que destruye la conexion a la base de datos que establecimos con el constructor
     */
    public function __destruct()
    {
        unset($this->PDO);
    }

    
    /**
     * Lee el contenido de los ficheros de confifuracion y devuelve un array con los datos necesarios para la conexion
     * 
     * @param String $xml_file Ruta donde se aloja el fichero de configuracion XML
     * @param String $xsd Ruta donde se aloja el fichero de configuracion XSD
     * 
     * @return Array Devuelve un array con los valores necesarios para establecer la conexion
     */
    function leer_configuracion($xml_file, $xsd)
    {

        $array = array();

        $conf = new \DOMDocument();
        $conf->load($xml_file);

        if (!$conf->schemaValidate($xsd)) {
            throw new \PDOException("Error de fichero");
        }

        $xml = simplexml_load_file($xml_file);

        $array['nombre'] = "" . $xml->xpath('//nombre')[0];
        $array['servidor'] = "" . $xml->xpath('//servidor')[0];
        $array['usuario'] = "" . $xml->xpath('//usuario')[0];
        $array['clave'] = "" . $xml->xpath('//clave')[0];

        return $array;
    }

    
    /**
     * Realiza la conexion a la base de datos con los datos de configuracion especificados en los ficheros
     * 
     * @return PDO Retorna la conexion a la base de datos
     */
     function conectar()
    {
        try {
            $pdo = new \PDO("mysql:host={$this->servidor};dbname={$this->nombreBBDD};charset=utf8", $this->usuario, $this->clave);
            return $pdo;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    

   

   


  

   

}


