<?php


require_once "../autoload.php";

use DB\Conexion as Conexion;

/**
 * 
 * Crea un tipo de habitacion en la base de datos dado los datos que se le pasan como argumentos
 * 
 * @param string $nombre_tipo   Nombre del tipo para añadir
 * @param string $descripcion   Descripcion del tipo de habitacion
 * @param int $metros_cuadrados Los metros cuadrados que ocupa ese tipo de habitacion
 * @param int $precio           El precio del tipo de habitacion
 * @param int $ventana          Si tiene ventana o no,1 es si 0 es no
 * @param int $internet         Si tiene internet o no,1 es si 0 es no
 * @param int $servicio_limpieza Si tiene servicio de limpieza o no,1 es si 0 es no
 * 
 * @return PDObject Devuelve el id del tipo de habitacion o un error si falla algo
 */
function crear_tipo_habitacion($nombre_tipo, $descripcion, $metros_cuadrados, $precio, $ventana, $internet, $servicio_limpieza)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("INSERT INTO tipo_habitaciones(tipo_de_habitacion, m2, descripcion,precio, ventana, internet, servicio_limpieza) VALUES (:tipo_de_habitacion,:m2,:descripcion,:precio,:ventana,:internet,:servicio_limpieza)");
        $sql->bindParam(":tipo_de_habitacion", $nombre_tipo);
        $sql->bindParam(":precio", $precio);
        $sql->bindParam(":m2", $metros_cuadrados);
        $sql->bindParam(":descripcion", $descripcion);
        $sql->bindParam(":ventana", $ventana);
        $sql->bindParam(":internet", $internet);
        $sql->bindParam(":servicio_limpieza", $servicio_limpieza);
        $sql->execute();

        if ($sql->rowCount() >= 1) {
            $id_tipo_habitacion = $db->PDO->lastInsertId();
        }

        $sql = null;
        $db = null;
        return $id_tipo_habitacion;
    } catch (PDOException $e) {
        die("Error al crear la habitacion" . $e->getMessage());
    }
}

/**
 * Comprueba si existe un tipo en la base de datos
 * 
 * @param string $tipo El tipo de habitacion que quieres buscar
 * 
 * @return boolean Devuelve true si lo a encontrado
 */
function buscar_tipo($tipo)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM tipo_habitaciones WHERE tipo_de_habitacion = :tipo");
        $sql->bindParam(":tipo", $tipo);
        $sql->execute();

        if ($sql->rowCount() >= 1) {
            return true;
        }
        $sql = null;
        $db = null;
    } catch (PDOException $e) {
        die("Error al crear la habitacion" . $e->getMessage());
    }
}

/**
 * @return Array Devuelde una lista de los datos de la tabla tipo_de_habitaciones
 * 
 * Devuelde una lista de los datos de la tabla tipo_de_habitaciones
 */
function listar_tipos_habitaciones()
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM tipo_habitaciones");
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los tipos de habitaciones" . $e->getMessage());
    }
}

/**
 * 
 * @param mixed $tipo_habitacion Nombre del tipo de habitacion
 * 
 * Inserta una habitacion del tipo que se le especifica
 */
function crear_habitacion($tipo_habitacion)
{
    try {
        $db = new Conexion;
        $db->PDO->beginTransaction();

        $sql = $db->PDO->prepare("INSERT INTO habitaciones(tipo_habitacion) VALUES (:tipo_habitacion)");
        $sql->bindParam(":tipo_habitacion", $tipo_habitacion);
        
        $db->PDO->commit();
        $sql->execute();
        if ($sql->rowCount() >= 1) {
            $id_habitacion = $db->PDO->lastInsertId();
        }

        $sql = null;
        $db = null;
        return $id_habitacion;
    } catch (PDOException $e) {
        die("Error al crear la habitacion" . $e->getMessage());
    }
}

/**
 * Devuelve las habitaciones de la base de datos
 * 
 * @return mixed    Devuelve todas las habitaciones o un error
 */
function listar_habitaciones()
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM habitaciones");
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}


/**
 * Elimina una habitacion con un id determinado
 * 
 * @param mixed $id_habitacion El id de la habitacion que quieras eliminar
 * 
 * 
 */
function eliminar_habitacion($id_habitacion)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("DELETE FROM habitaciones WHERE id = :id_habitacion");
        $sql->bindParam(":id_habitacion", $id_habitacion);
        $sql->execute();

        $sql = null;
        $db = null;
    } catch (PDOException $e) {
        die("Error al eliminar la habitacion" . $e->getMessage());
    }
}

/**
 * Asigna un servicio a un tipo de habitacion
 * 
 * @param mixed $id_servicio Id del servicio para añadir
 * @param mixed $id_habitacion  Id del tipo de habitacion a la que añadir el servicio
 * 
 */
function asignar_servicio($id_servicio, $id_habitacion)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("INSERT INTO habitacion_servicio (id_habitacion,id_servicio) VALUES (:id_habitacion, :id_servicio)");
        $sql->bindParam(":id_habitacion", $id_habitacion);
        $sql->bindParam(":id_servicio", $id_servicio);
        $sql->execute();
        $sql = null;

        $db =  null;
    } catch (PDOException $e) {
        die("Error al crear el usuario" . $e->getMessage());
    }
}


/**
 * Asigna una ruta de imagen a un tipo de habitacion 
 * 
 * @param mixed $id_tipo_habitacion Id del tipo de habitacion a la que se le añade la imagen
 * @param string $imagen_habitacion Ruta de la imagen relacionada
 * 
 */
function asignar_imagen($id_tipo_habitacion, $imagen_habitacion){
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("INSERT INTO imagenes_habitaciones (id_tipo_habitacion, imagen_habitacion) VALUES (:id_tipo_habitacion, :imagen_habitacion)");
        $sql->bindParam(":id_tipo_habitacion", $id_tipo_habitacion);
        $sql->bindParam(":imagen_habitacion", $imagen_habitacion);
        $sql->execute();
        $sql = null;
        $db =  null;
    } catch (PDOException $e) {
        die("Error al crear el usuario" . $e->getMessage());
    }
}

/**
 * Muestra las imagenes relacionadas con el id del tipo de habitacion
 * 
 * @param mixed $id_tipo_habitacion Id del tipo de habitacion que corresponde con las imagenes
 * 
 * @return [type]
 */
function visualizar_imagenes($id_tipo_habitacion)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM imagenes_habitaciones WHERE id_tipo_habitacion = :id_tipo_habitacion");
        $sql->bindParam(":id_tipo_habitacion", $id_tipo_habitacion);
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}

function recoger_datos_tipo($nombre_tipo)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM tipo_habitaciones WHERE tipo_de_habitacion = :tipo_de_habitacion");
        $sql->bindParam(":tipo_de_habitacion", $nombre_tipo);
        $sql->execute();

        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
        
    } catch (PDOException $e) {
        die("Error al buscar el tipo de habitacion" . $e->getMessage());
    }
}

function modificar_tipo_habitacion($nombre_tipo,$m2,$descripcion,$precio,$internet,$ventana,$servicio_limpieza){
    try{

        $db = new Conexion;        
        $sql = $db->PDO->prepare("UPDATE tipo_habitaciones SET m2 = :m2, descripcion = :descripcion, precio = :precio, internet = :internet, ventana = :ventana, servicio_limpieza = :servicio_limpieza WHERE tipo_de_habitacion = :tipo_de_habitacion");
        $sql->bindParam(":tipo_de_habitacion", $nombre_tipo);
        $sql->bindParam(":m2", $m2);
        $sql->bindParam(":descripcion", $descripcion);
        $sql->bindParam(":precio", $precio);
        $sql->bindParam(":internet", $internet);
        $sql->bindParam(":ventana", $ventana);
        $sql->bindParam(":servicio_limpieza", $servicio_limpieza);
        $sql->execute();

        $sql = null;
        $db =  null;

    }catch(PDOException $e){
        die("Error al crear el usuario". $e->getMessage());
    }

}
