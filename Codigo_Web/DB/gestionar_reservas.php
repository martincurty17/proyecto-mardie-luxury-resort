<?php
require_once "../autoload.php";

use DB\Conexion as Conexion;

/**
 * 
 * Comprueba la cantidad de reservas que se quieren hacer y si es posible hacerlas
 * 
 * @param int $cantidad Cantidad de reservas que se quieren realizar a cabo
 * @param mixed $tipo_habitacion El tipo de habitacion al que asignar las reservas
 * 
 * @return array    Devuelve un array con los id de las habitaciones reservadas
 */
function comprobar_cantidad($cantidad, $tipo_habitacion)
{
    try {
        $db = new Conexion;
        /**
         * En una misma consulta comprueba la cantidad de habitaciones por tipo y 
         * devuelve los ids
         */
        $sql = $db->PDO->prepare("SELECT id FROM habitaciones WHERE tipo_habitacion = :tipo_habitacion");
        $sql->bindParam(":tipo_habitacion", $tipo_habitacion);
        $sql->execute();
        if ($sql->rowCount() && $sql->rowCount() >= $cantidad) {
            $ids_habitaciones = [];
            $resultado = $sql->fetchAll();
            for ($i = 0; $i < $cantidad; $i++) {
                $ids_habitaciones[] = $resultado[$i]["id"];
            }
            return $ids_habitaciones;
        } else {
            echo "No se puede";
        }
        $sql = null;
        $db =  null;
    } catch (PDOException $e) {
        die("Error al comprobar la cantidad" . $e->getMessage());
    }
}


/**
 * Crea una reserva de una habitacion
 * 
 * @param string $id_usuario    Id del usuario que realiza la reserva
 * @param string $fecha_entrada Fecha en la que comienza la reserva
 * @param string $fecha_salida  Fecha en la que termina la reserva
 * @param string $tipo_habitacion   El tipo de habitacion reservada
 * 
 * @return mixed    Si todo sale bien devuelve el id de la reserva si no devuelve Null
 */
function crear_reserva($id_usuario, $fecha_entrada, $fecha_salida, $tipo_habitacion)
{
    try {
        $id_reserva = null;
        $db = new Conexion;
        $sql = $db->PDO->prepare("INSERT INTO reservas (id_usuario, fecha_entrada, fecha_salida, tipo_habitacion) VALUES (:id_usuario,:fecha_entrada,:fecha_salida,:tipo_habitacion)");
        $sql->bindParam(":id_usuario", $id_usuario);
        $sql->bindParam(":fecha_entrada", $fecha_entrada);
        $sql->bindParam(":fecha_salida", $fecha_salida);
        $sql->bindParam(":tipo_habitacion", $tipo_habitacion);
        $sql->execute();
        if ($sql->rowCount() >= 1) {
            $id_reserva = $db->PDO->lastInsertId();
        }

        $sql = null;
        $db =  null;
        return $id_reserva;
    } catch (PDOException $e) {
        die("Error al crear la reserva" . $e->getMessage());
    }
}

/**
 * Asigna una reserva a una habitacion
 * 
 * @param string $num_reserva   El numero de reserva que es
 * @param array $ids_habitacion Los id de las habitaciones que se van a asignar a una reserva
 * 
 */
function asignar_habitacion($num_reserva, $ids_habitacion)
{
    try {
        $db = new Conexion;
        foreach ($ids_habitacion as $id_habitacion) {
            $sql = $db->PDO->prepare("INSERT INTO habitaciones_reservas (num_reserva,id_habitacion) VALUES (:num_reserva, :id_habitacion)");
            $sql->bindParam(":num_reserva", $num_reserva);
            $sql->bindParam(":id_habitacion", $id_habitacion);
            $sql->execute();
            $sql = null;
        }
        $db =  null;

    } catch (PDOException $e) {
        die("Error al asignar habitacion" . $e->getMessage());
    }
}

/**
 * 
 * Elimina la reserva que se le pasa por cabecera
 * 
 * @param mixed $num_reserva
 * 
 * @return [type]
 */
function eliminar_reserva($num_reserva){
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("DELETE FROM habitaciones_reservas WHERE num_reserva = :num_reserva; DELETE FROM reservas WHERE num_reserva = :num_reserva");
        $sql->bindParam(":num_reserva", $num_reserva);
        $sql->execute();

        $sql = null;
        $db = null;
    } catch (PDOException $e) {
        die("Error al eliminar la reserva" . $e->getMessage());
    }
}
