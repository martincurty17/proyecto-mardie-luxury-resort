<?php

require_once "../autoload.php";

use DB\Conexion as Conexion;

/**
 * @param mixed $nombre_servicio Parametro por el cual busca un servicio 
 * 
 * @return Bool Verdadero o False en funcion de si el servicio ha sido encontrado
 * 
 * Funcion que devuelve un servicio con todos sus datos de la base de datos.
 * Se busca el servicio en funcion al nombre_servicio
 */
function buscar_servicio($nombre_servicio)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM servicios WHERE nombre_servicio = :nombre_servicio");
        $sql->bindParam(":nombre_servicio", $nombre_servicio);
        $sql->execute();

        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        $encontrado = true;

        if (empty($resultado)) {
            $encontrado = false;
            return $encontrado;
        } else {
            echo "Ya existe un servicio con ese nombre";
        }
    } catch (PDOException $e) {
        die("Error al buscar el servicio" . $e->getMessage());
    }
}


/**
 * Devuelve toda la informacion de un servicio segun si nombre
 * 
 * @param string $nombre_servicio   Nombre del servicio 
 * 
 * @return mixed    Devuelve todos los datos de un servicio segun su nombre o si no devuelve null
 */
function recoger_datos_servicio($nombre_servicio)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM servicios WHERE nombre_servicio = :nombre_servicio");
        $sql->bindParam(":nombre_servicio", $nombre_servicio);
        $sql->execute();

        $resultado = $sql->fetch(PDO::FETCH_ASSOC);
        return $resultado;
        
    } catch (PDOException $e) {
        die("Error al buscar el servicio" . $e->getMessage());
    }
}

/**
 * @param mixed $nombre_servicio Nombre del servicio
 * @param mixed $precio_servicio Precio del Servicio
 * @param mixed $descripcion_servicio Descripcion detallada del servicio
 * @param mixed $disponibilidad_servicio Disponibilidad del servicio
 * 
 * Funcion encargada de recibir los valores del formulario para
 * insertar un nuevo servicio en la base de datos
 * 
 */
function crear_servicio($nombre_servicio, $precio_servicio, $descripcion_servicio, $disponibilidad_servicio)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("INSERT INTO servicios (nombre_servicio, precio_servicio, descripcion, disponibilidad) VALUES (:nombre_servicio, :precio_servicio, :descripcion_servicio, :disponibilidad_servicio)");
        $sql->bindParam(":nombre_servicio", $nombre_servicio);
        $sql->bindParam(":precio_servicio", $precio_servicio);
        $sql->bindParam(":descripcion_servicio", $descripcion_servicio);
        $sql->bindParam(":disponibilidad_servicio", $disponibilidad_servicio);
        $sql->execute();

        $sql = null;
        $db = null;
    } catch (PDOException $e) {
        die("Error al crear el servicio" . $e->getMessage());
    }
}

/**
 * @return Array Lista de todos los servicios existentes en la base de datos
 * 
 * La funcion se encarga de recoger todos los servicios almacenados en la base de datos
 * para poder mostrarlos por pantalla
 */
function listar_servicios()
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM servicios");
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}

/**
 * @param mixed $nombre_servicio Nombre del servicio
 * 
 * Funcion que permite eliminar un servicio de la base de datos.
 * 
 */
function eliminar_servicios($nombre_servicio)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("DELETE FROM servicios WHERE nombre_servicio = :nombre_servicio");
        $sql->bindParam(":nombre_servicio", $nombre_servicio);
        $sql->execute();

        $sql = null;
        $db = null;
    } catch (PDOException $e) {
        die("Error al eliminar los servicios" . $e->getMessage());
    }
}
/**
 * Modifica un servicio segun los datos pasados como argumentos
 * 
 * @param string $nombre_servicio   Nuevo nombre para el servicio
 * @param string $precio_servicio   Nuevo precio para el servicio
 * @param string $descripcion_servicio  Nueva descripcion para el servicio
 * @param string $disponibilidad_servicio   Nueva disponibilidad para el servicio
 * 
 * @return null Devuelve null si hay algun fallo
 */
function modificar_servicio($nombre_servicio, $precio_servicio, $descripcion_servicio, $disponibilidad_servicio){
    try{

        $db = new Conexion;        
        $sql = $db->PDO->prepare("UPDATE servicios SET precio_servicio = :precio_servicio, descripcion = :descripcion_servicio, disponibilidad = :disponibilidad_servicio WHERE nombre_servicio = :nombre_servicio");
        $sql->bindParam(":nombre_servicio", $nombre_servicio);
        $sql->bindParam(":precio_servicio", $precio_servicio);
        $sql->bindParam(":descripcion_servicio", $descripcion_servicio);
        $sql->bindParam(":disponibilidad_servicio", $disponibilidad_servicio);
        $sql->execute();

        $sql = null;
        $db =  null;

    }catch(PDOException $e){
        die("Error al crear el usuario". $e->getMessage());
    }
}
