<?php

require_once "../autoload.php";

use DB\Conexion as Conexion;

/**
 * @param mixed $nombre Nombre del usuario
 * @param mixed $password Contraseña del usuario
 * @param mixed $email Correo del usuario
 * @param mixed $telf Telefono del usuario
 * @param mixed $direccion Direccion del usuario
 * @param int $rol Rol del usuario
 * 
 * Funcion que se encarga de registrar un nuevo usuario en la base de datos
 */
function registrar_usuario($nombre, $password, $email, $telf, $direccion, $rol = 1)
{
    try{
        $db = new Conexion;        
        $sql = $db->PDO->prepare("INSERT INTO usuarios (nombre, email, telf, direccion, password, rol_usuario) VALUES (:nombre,:email,:telf,:direccion,:password,:rol)");
        $sql->bindParam(":nombre", $nombre);
        $sql->bindParam(":email", $email);
        $sql->bindParam(":telf", $telf);
        $sql->bindParam(":direccion", $direccion);
        $sql->bindParam(":password", $password);
        $sql->bindParam(":rol", $rol);  
        $sql->execute();

        $sql = null;
        $db =  null;

    }catch(PDOException $e){
        die("Error al crear el usuario". $e->getMessage());
    }
}

/**
 * @param mixed $nombreLogin Nombre del usuario ya registrado
 * @param mixed $passwordLogin Contraseña del usuario
 * 
 * Funcion que comprueba si existe el usuario en la base de datos
 * Y si existe permite hacer login y crear una sesion
 * 
 * @return boolean Devuelve true si se ha iniciado sesion correctamente
 */
function login_usuario($nombreLogin, $passwordLogin){
    try{
        $db = new Conexion;        
        $sql = $db->PDO->prepare("SELECT * FROM usuarios WHERE nombre = :nombreLogin");
        $sql->bindParam(":nombreLogin", $nombreLogin);
        $sql->execute();
        $resultado = $sql->fetch(PDO::FETCH_ASSOC);

        if(!empty($resultado)){
            if(password_verify($passwordLogin, $resultado['password'])){
                
                crear_sesion($resultado);
                return true;
            }else{  
                echo "Error en la contraseña";
                return false;
            }
        }else{
            return false;
            echo "No existe el usuario";
        }
    }catch(PDOException $e){
        die("Error al buscar el usuario en la base de datos". $e->getMessage());
    }
}

/**
 * 
 * Funcion que devuelve todos los usuarios
 * 
 * @return Array Devuelve un array si no hay ningun error
 */
function listar_usuarios(){
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM usuarios");
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los Usuarios" . $e->getMessage());
    }
}

/**
 * @param mixed $resultado Array con los valores del usuario
 * 
 * @return Session Devuelve una sesion con los datos del usuario
 * 
 * Crea una nueva sesion con los datos del usuario
 */
function crear_sesion($resultado) {
    $_SESSION["id_usuario"] = $resultado["id"];
    $_SESSION["nombre"] = $resultado["nombre"];
    $_SESSION["rol"] = $resultado["rol_usuario"];
    $_SESSION["email"] = $resultado["email"];
    $_SESSION["telf"] = $resultado["telf"];
    $_SESSION["direccion"] = $resultado["direccion"];
}

/**
 * Cierra todas las sesiones
 */
function cerrar_sesion(){
    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_start();
    }
    session_unset();
    session_destroy();    
}



/**
 * Devuelve el nombre del usuario que esta en la sesion
 * 
 * @return string Devuelve el nombre del usuario o un mensaje en caso de que este vacio
 */
function get_usuario_sesion(){
    if(!empty($_SESSION["nombre"])){
        return $_SESSION["nombre"];
    }else{
        return "No hay usuario";
    }
}

/**
 * Modifica los atributos de un usuario que se le pasan por cabecera
 * 
 * @param string $nombre    Nuevo Nombre de usuario
 * @param string $password  Nueva Contraseña del usuario
 * @param string $email     Nuevo Email del usuario
 * @param string $telf      Nuevo Telefono del usuario
 * @param string $direccion Nueva Direccion del usuario
 * @param string $id_usuario    Nuevo Id del usuario a modificar
 * 
 * 
 */
function modificar_perfil($nombre, $password, $email, $telf, $direccion,$id_usuario){
    try{
        $db = new Conexion;        
        $sql = $db->PDO->prepare("UPDATE usuarios SET nombre = :nombre, email = :email, telf = :telf, direccion= :direccion, password = :password WHERE id = :id_usuario ");
        $sql->bindParam(":nombre", $nombre);
        $sql->bindParam(":email", $email);
        $sql->bindParam(":telf", $telf);
        $sql->bindParam(":direccion", $direccion);
        $sql->bindParam(":password", $password);
        $sql->bindParam(":id_usuario", $id_usuario);
        $sql->execute();

        $sql = null;
        $db =  null;

    }catch(PDOException $e){
        die("Error al crear el usuario". $e->getMessage());
    }
}

function listar_reservas_usuario($id_usuario)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM reservas WHERE id_usuario = :id_usuario");
        $sql->bindParam(":id_usuario", $id_usuario);
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}

function listar_reservas_usuarios()
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT * FROM reservas");
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}

function buscar_usuario_id($id_usuario)
{
    try {
        $db = new Conexion;
        $sql = $db->PDO->prepare("SELECT nombre FROM usuarios WHERE id = :id_usuario");
        $sql->bindParam(":id_usuario", $id_usuario);
        $sql->execute();
        $resultado = $sql->fetchAll();

        $sql = null;
        $db = null;
        return $resultado;
    } catch (PDOException $e) {
        die("Error al listar los servicios" . $e->getMessage());
    }
}

