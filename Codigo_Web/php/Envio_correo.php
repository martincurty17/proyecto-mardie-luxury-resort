<?php

use PHPMailer\PHPMailer\PHPMailer;

require dirname(__FILE__) . "/../vendor/autoload.php";

/**
 * @param mixed $correo Email al que se le va a enviar el correo
 * @param mixed $nombre Nombre del Usuario destinatario
 * 
 * @return function Devuelve una funcion que se encarga de generar el correo
 * 
 * Añade los parametros del correo y el nombre del usuario al envio
 */
function enviar_correos($correo, $nombre) {
    
    $cuerpo = crear_correo($correo, $nombre);
    $correo_Departamento_Pedidos = "Mardie Luxury Resort"; 
    return enviar_correo("$correo, $correo_Departamento_Pedidos",
            $cuerpo, "Mardie Luxury Resort");
}
/**
 * @param mixed $correo Email al que se le va a enviar el correo
 * @param mixed $nombre Nombre del Usuario destinatario
 * 
 * @return function Devuelve una funcion que se encarga de generar el correo
 * 
 * Añade los parametros del correo y el nombre del usuario al envio
 */
function enviar_correo_habitacion($correo, $nombre) {
    
    $cuerpo = crear_correo_confirmacion($correo, $nombre);
    $correo_Departamento_Pedidos = "Mardie Luxury Resort"; 
    return enviar_correo("$correo, $correo_Departamento_Pedidos",
            $cuerpo, "Mardie Luxury Resort");
}

/**
 * @param mixed $correo Email al que se le va a enviar el correo
 * @param mixed $nombre Nombre del Usuario destinatario
 * 
 * @return function Devuelve una funcion que se encarga de generar el correo
 * 
 * Añade los parametros del correo y el nombre del usuario al envio
 */
function enviar_correo_admin($correo,$reserva) {
    
    $cuerpo = crear_correo_admin($correo, $reserva);
    $correo_Departamento = "Mardie Luxury Resort"; 
    return enviar_correo("mardieluxuryhotel@gmail.com, $correo_Departamento",
            $cuerpo, "Mardie Luxury Resort");
}

/**
 * @param mixed $correo Correo recibido del formulario
 * @param mixed $nombre Nombre del usuario
 * 
 * @return String Formato del correo electronico
 * 
 * Genera el formato del correo electronico que se envia al usuario
 */
function crear_correo_admin($correo, $reserva) {
    $texto = "<h1 class='titulo'> Mardie Luxury Resort</h1>";
    $texto .= "Nueva peticion de reserva:";
    $texto .= "<p>Numero de reserva: $reserva</p>";
    $texto .= "<p>La peticion de la reserva se hizo con el correo:   $correo  </p>";
    $texto .= "<a href='http://localhost/proyecto-mardie-luxury-resort/Codigo_Web/php/index.php' style='text-decoration: none; color:blue;'>Confirmar Reserva</a>";
    return $texto;
}

/**
 * @param string $correo Correo a donde se envia el mensaje
 * @param string $nombre El nombre de Usuario que reserva una habitacion
 * 
 * @return string Devuelve el cuerpo del correo 
 */
function crear_correo_confirmacion($correo, $nombre) {
    $texto = "<h1 class='titulo'> Mardie Luxury Resort</h1>";
    $texto .= "Reserva realizada con exito:";
    $texto .= "<p>Estimado ciente ,$nombre</p>";
    $texto .= "<p>Su reserva en nuestro hotel, a traves del correo  $correo  se ha completado con exito </p>";
    $texto .= "<p>Gracias por confiar en nosotros.</p>";
    return $texto;
}

/**
 * @param mixed $correo Correo recibido del formulario
 * @param mixed $nombre Nombre del usuario
 * 
 * @return String Formato del correo electronico
 * 
 * Genera el formato del correo electronico que se envia al usuario
 */
function crear_correo($correo, $nombre) {
    $texto = "<h1 class='titulo'> Mardie Luxury Resort</h1>";
    $texto .= "Registro exitoso:";
    $texto .= "<p>Estimado ciente ,$nombre</p>";
    $texto .= "<p>Su registro en nuestra página web a traves del correo  $correo  se ha completado con exito </p>";
    $texto .= "<p>Gracias por confiar en nosotros.</p>";
    return $texto;
}

/**
 * @param mixed $lista_correos Lista de correos
 * @param mixed $cuerpo Cuerpo del correo electronico
 * @param $asunto Asunto del correo
 * 
 * @return Bool Devuelve verdadero si se ha enviado correctamente o error en caso contrario
 */
function enviar_correo($lista_correos, $cuerpo, $asunto = "Registro Exitoso") {
    $res = leer_configCorreo(dirname(__FILE__) . "/../config/correo.xml", dirname(__FILE__) . "/../config/correo.xsd");
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;  // cambiar a 1 o 2 para ver errores
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "tls";
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->Username = $res[0];  //usuario de gmail
    $mail->Password = $res[1]; //contraseña de gmail          
    $mail->SetFrom('usuario_correo@gmail.com', 'Mardie Luxury Resort');
    $mail->Subject = utf8_decode($asunto);
    $mail->MsgHTML($cuerpo);
    /* Divide la lista de correos por la coma */
    $correos = explode(",", $lista_correos);
    foreach ($correos as $correo) {
        $mail->AddAddress($correo, $correo);
    }
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return TRUE;
    }
}

/**
 * @param mixed $nombre Nombre del fichero
 * @param mixed $esquema Esquema de configuracion
 * 
 * @return Array devuelve un array con el resultado
 */
function leer_configCorreo($nombre, $esquema) {
    /*
     * Recibe dos parámetros: 
     * 1. $nombre: fichero de configuración con los datos (usuario y clave) para enviar un correo
     * 2. $esquema: fichero de validación,para comprobar la estructura que se espera
     * del fichero de configuración 
     */
    $config = new DOMDocument();
    $config->load($nombre);
    //Validamos el documento basandonos en el esquema
    $res = $config->schemaValidate($esquema);
    if ($res === FALSE) {
        throw new InvalidArgumentException("Revise fichero de configuración");
    }
    //Interpretamos el fichero xml
    $datos = simplexml_load_file($nombre);
    //Buscamos el SimpleXML entre los hijos que cumpla 
    $usu = $datos->xpath("//usuario");
    $clave = $datos->xpath("//clave");
    $resul = [];
    $resul[] = $usu[0];
    $resul[] = $clave[0];
    return $resul;
}

?>