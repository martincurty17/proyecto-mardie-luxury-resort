<?php
require_once '../DB/gestionar_habitaciones.php';
require_once '../DB/gestionar_servicios.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $ventana = false;
    $internet = false;
    $servicio_limpieza = false;

    if (isset($_POST["crear_habitacion"])) {

        crear_habitacion($_POST['select_habitaciones']);
    }

    if (isset($_POST["eliminar_habitacion"])) {
        foreach ($_POST['check_habitacion'] as $checked) {
            eliminar_habitacion($checked);
        }
    }

    if (isset($_POST["modificar_tipo"])) {
        header("Location: Modificar_habitaciones.php?nombre=".$_POST['select_tipo']);
    }


    if (isset($_POST["crear_tipo"])) {

        if (!empty($_POST["nombre_tipo"]) && !empty($_POST["descripcion"]) && !empty($_POST["precio"]) && $_POST["precio"] > 0) {
            if (!empty($_POST["metros_cuadrados"]) && is_numeric($_POST["metros_cuadrados"]) && $_POST["metros_cuadrados"] > 0) {
                if (!empty($_POST["ventana"])) {
                    $ventana = 1;
                }
                if (!empty($_POST["internet"])) {
                    $internet = 1;
                }
                if (!empty($_POST["servicio_limpieza"])) {
                    $servicio_limpieza = 1;
                }
                if (!buscar_tipo($_POST["nombre_tipo"])) {
                    $id_tipo = crear_tipo_habitacion($_POST["nombre_tipo"], $_POST["descripcion"], $_POST["metros_cuadrados"], $_POST["precio"], $ventana, $internet, $servicio_limpieza);
                    $image = $_FILES["imagen_tipo_habitacion"]['name'];
                    print_r($image);
                    $ruta = "../img/".$image;
                    $imgsave=$_FILES["imagen_tipo_habitacion"]['tmp_name'];
                   if (move_uploaded_file($imgsave,$ruta)) {
                       print_r("La imagen se a subido correctamente");
                   }else{
                    print_r("No se ha podido subir la imagen");
                   }
                    asignar_imagen($id_tipo, $ruta);
                } else {
                    echo "Ya existe un tipo con ese nombre";
                }
            }
        }
    }
    if (isset($_POST["add_servicio"])) {
        foreach ($_POST['check_habitacion'] as $checked) {
            asignar_servicio($_POST["servicio"], $checked);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <link rel="stylesheet" href="../styles/hab/estilos.css">
    <title>Crear Habitaciones</title>
</head>

<body>
<div id="cuerpesito">
    <div></div>
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" enctype="multipart/form-data" method="POST" id="formulario_crear_habitaciones" name="formulario_crear_habitaciones">
    <h3>Crear tipo de habitación</h3>   
      <label for="">Nombre Tipo: </label><br>
        <input type="text" id="nombre_tipo" name="nombre_tipo" required><br>

        <label for="">Precio: </label><br>
        <input type="number" id="precio" name="precio" required><br>

        <label for="">Metros cuadrados: </label><br>
        <input type="number" id="metros_cuadrados" name="metros_cuadrados" min=10 required><br>

        <label for="">Descripcion: </label><br>
        <textarea name="descripcion" id="descripcion" cols="15" rows="5" required></textarea><br>

        <label for="">Ventana</label>
        <input type="checkbox" id="ventana" name="ventana"><br>

        <label for="">Internet</label>
        <input type="checkbox" id="internet" name="internet"><br>

        <label for="">Servicio de Limpieza</label>
        <input type="checkbox" id="limpieza" name="limpieza"><br><br>

        <input type="file" id="imagen_tipo_habitacion" name="imagen_tipo_habitacion" multiple required><br>

        <input type="submit" name="crear_tipo" id="crear_tipo" value=" + Añadir Tipo">
    </form>
    <form  id="blanco"action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST">
        <h3>Lista de Habitaciones</h3>    
        <select name="servicio" id="servicio">
                <?php
                $servicios = listar_servicios();
                if (!empty($servicios)) {
                    foreach ($servicios as $servicio) {
                        print_r("<option value='" . $servicio["id"] . "'>" . $servicio["nombre_servicio"] . "</option>");
                    }
                } else {
                    print_r("<option>No existen Servicios</option>");
                }
                ?>
            </select><br>
            <?php
            $array_habitaciones = listar_habitaciones();
            foreach ($array_habitaciones as $habitacion) {
                print_r("<input type='checkbox' id='check_habitacion' name='check_habitacion[" . $habitacion["id"] . "]' value='" . $habitacion["id"] . "'> " . $habitacion["id"] . " " . $habitacion["tipo_habitacion"] . "</br>");
            }
            ?>

            <input type="submit" id="eliminar_habitacion" name="eliminar_habitacion" value="Eliminar">
            <input type="submit" id="add_servicio" name="add_servicio" value="Añadir Servicio">
        </form> 

 <form class="modificar" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_habitaciones" name="formulario_habitaciones">
    <h3>Modificar Tipo</h3>
        <select id="select_tipo" name="select_tipo">
            <?php

            $array_tipo_habitaciones = listar_tipos_habitaciones();
            if (!empty($array_tipo_habitaciones)) {
                foreach ($array_tipo_habitaciones as $tipo_habitacion) {
                    print_r("<option value='" . $tipo_habitacion["tipo_de_habitacion"] . "'>" . $tipo_habitacion["tipo_de_habitacion"] . "</option>");
                }
            } else {
                print_r("<option>No existen tipos</option>");
            }

            ?>
        </select>
        <input type="submit" name="modificar_tipo" id="modificar_tipo" value="Modificar Tipo">
    </form>
   
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_habitaciones" name="formulario_habitaciones">
      <h3>Crear Habitación</h3>  
       <select id="select_habitaciones" name="select_habitaciones">
            <?php

            $array_tipo_habitaciones = listar_tipos_habitaciones();
            if (!empty($array_tipo_habitaciones)) {
                foreach ($array_tipo_habitaciones as $tipo_habitacion) {
                    print_r("<option value='" . $tipo_habitacion["tipo_de_habitacion"] . "'>" . $tipo_habitacion["tipo_de_habitacion"] . "</option>");
                }
            } else {
                print_r("<option>No existen tipos</option>");
            }

            ?>
        </select>
        <input type="submit" name="crear_habitacion" id="crear_habitacion" value=" + Añadir Habitación">
    </form>
        
           
       
    <div></div>
        </div>
    
        <a href="./Index.php"  style="text-decoration: none; color:white;"> De vuelta a casa</a>
</body>

</html>