<?php
require_once '../DB/gestionar_habitaciones.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $datos = recoger_datos_tipo($_GET["nombre"]);    
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST["modificar_tipo"])) {
        if (!empty($_POST["ventana"])) {
            $ventana = 1;
        }
        if (!empty($_POST["internet"])) {
            $internet = 1;
        }
        if (!empty($_POST["servicio_limpieza"])) {
            $servicio_limpieza = 1;
        }
        modificar_tipo_habitacion($_POST["nombre_tipo"], $_POST["metros_cuadrados"], $_POST["descripcion"], $_POST["precio"],$internet,$ventana,$servicio_limpieza);
        header("Location: Habitaciones.php");
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <title>Modificar Tipo</title>
</head>

<body>
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_modificar_tipo" name="formulario_modificar_tipo">
    <label for="">Nombre Tipo: </label><br>
        <input type="text" id="nombre_tipo" name="nombre_tipo" value="<?php echo $datos["tipo_de_habitacion"] ?>" required><br>

        <label for="">Precio: </label><br>
        <input type="number" id="precio" name="precio" value="<?php echo $datos["precio"] ?>" required><br>

        <label for="">Metros cuadrados: </label><br>
        <input type="number" id="metros_cuadrados" name="metros_cuadrados" min=10 value="<?php echo $datos["m2"] ?>" required><br>

        <label for="">Descripcion: </label><br>
        <textarea name="descripcion" id="descripcion" cols="15" rows="5" value="<?php echo $datos["descripcion"] ?>" required></textarea><br>

        <label for="">Ventana</label>
        <input type="checkbox" id="ventana" name="ventana"><br>

        <label for="">Internet</label>
        <input type="checkbox" id="internet" name="internet"><br>

        <label for="">Servicio de Limpieza</label>
        <input type="checkbox" id="limpieza" name="limpieza"><br><br>

        <input type="submit" name="modificar_tipo" id="modificar_tipo" value="Modificar Tipo">
    </form>
</body>

</html>