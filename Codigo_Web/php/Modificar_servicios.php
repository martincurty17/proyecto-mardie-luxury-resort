<?php
require_once '../DB/gestionar_servicios.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $datos = recoger_datos_servicio($_GET["nombre"]);    
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST["modificar_servicio"])) {
        $disponibilidad_servicio=0;
        if(!empty($_POST["disponibilidad"])){
            $disponibilidad_servicio=1;
        }
        
        modificar_servicio($_GET["nombre"], $_POST["precio_servicio"], $_POST["descripcion_servicio"], $disponibilidad_servicio);
        header("Location: Servicios.php");
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <title>Modificar Servicio</title>
</head>

<body>
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_modificar_servicio" name="formulario_modificar_servicio">
        <label for="nombre_servicio">Nombre</label><br>
        <input type="text" id="nombre_servicio" name="nombre_servicio" value="<?php echo $datos["nombre_servicio"] ?>" disabled><br>
        <label for="precio_servicio">Precio</label><br>
        <input type="text" id="precio_servicio" name="precio_servicio" value="<?php echo $datos["precio_servicio"] ?>" required><br>
        <label for="descripcion_servicio">Descripcion</label><br>
        <input type="text" id="descripcion_servicio" name="descripcion_servicio" value="<?php echo $datos["descripcion"] ?>" required><br>
        <label for="disponibilidad_servicio">Disponibilidad</label><br>
        <input type="checkbox" id="disponibilidad_servicio" name="disponibilidad_servicio" <?php if ($datos["disponibilidad"] == 1) {
                                                                                                echo "checked";
                                                                                            } ?>><br>
        <input type="submit" id="modificar_servicio" name="modificar_servicio" value="Modificar">
    </form>
</body>

</html>