<?php
require_once '../DB/gestionar_reservas.php';
require_once '../DB/gestionar_habitaciones.php';
require_once 'Envio_correo.php';
require_once '../DB/gestionar_usuarios.php';

session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (isset($_POST["crear_reserva"])) {
        $array_ids_habitaciones = comprobar_cantidad($_POST["cantidad_habitaciones"], $_POST["tipo_habitacion"]);
        if ($array_ids_habitaciones != null) {
            $numero_reserva = crear_reserva($_SESSION["id_usuario"], $_POST["fecha_entrada"], $_POST["fecha_salida"], $_POST["tipo_habitacion"]);
            if ($numero_reserva != null) {
                asignar_habitacion($numero_reserva, $array_ids_habitaciones);
                enviar_correo_habitacion($_SESSION["email"], $_SESSION["nombre"]);
                enviar_correo_admin($_SESSION["email"],$numero_reserva);
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <title>Reservas</title>
</head>

<body>
    Usuario
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_reservas" name="formulario_reservas">
        <?php
        if ($_SESSION["rol"] == 2) {
        ?>
            <select name="usuario" id="usuario">
                <?php
                $usuarios = listar_usuarios();
                if (!empty($usuarios)) {
                    foreach ($usuarios as $usuario) {
                        print_r("<option value='" . $usuario["id"] . "'>" . $usuario["nombre"] . "</option>");
                    }
                } else {
                    print_r("<option>No existen usuarios</option>");
                }
                ?>
            </select><br>
        <?php
        } else {
        ?>
            <input type="text" id="usuario" name="usuario" value="<?php echo get_usuario_sesion() ?>" disabled><br><br>
        <?php
        } ?>
        Tipo de Habitacion
        <select name="tipo_habitacion" id="tipo_habitacion">
          
          
            <?php
            if(isset($_SESSION['rol'])&&$_SESSION['rol']==2){
                $array_tipo_habitaciones = listar_tipos_habitaciones();
                if (!empty($array_tipo_habitaciones)) {
                    foreach ($array_tipo_habitaciones as $tipo_habitacion) {
                        print_r("<option value='" . $tipo_habitacion["tipo_de_habitacion"] . "'>" . $tipo_habitacion["tipo_de_habitacion"] . "</option>");
                    }
                } else {
                    print_r("<option>No existen tipos</option>");
                }
            }else{
                print_r("<option value='" . $_POST["habitacion"] . "'>" . $_POST["habitacion"] . "</option>");
            }

            ?>
             
        </select><br><br>
        Numero de Habitaciones:<input type="number" id="cantidad_habitaciones" name="cantidad_habitaciones" default=1><br><br>
        Fecha de Entrada <br>
        <input type="date" id="fecha_entrada" name="fecha_entrada" min="2020-06-09" required><br><br>
        Fecha de Salida <br>
        <input type="date" id="fecha_salida" name="fecha_salida" required><br><br>

        <input type="submit" id="crear_reserva" name="crear_reserva" value="Crear Reserva">
    </form>
    <br>
    <a href="./Index.php"  style="text-decoration: none; color:white;"> De vuelta a casa</a>
</body>

</html>