<?php

require_once '../DB/gestionar_servicios.php';
session_start();



if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (isset($_POST["nuevo_servicio"])) {
        $nombre_servicio = $_POST["nombre_servicio"];
        $precio_servicio = $_POST["precio_servicio"];
        $descripcion_servicio = $_POST["descripcion_servicio"];
        if (!empty($_POST["disponibilidad_servicio"])) {
            $disponibilidad_servicio = 1;
        }

        if (!empty($_POST["nombre_servicio"]) && !empty($_POST["precio_servicio"]) && !empty($_POST["descripcion_servicio"])) {

            if (buscar_servicio($nombre_servicio) == false) {
                /**
                 * Se puede continuar creando el servicio
                 */
                crear_servicio($nombre_servicio, $precio_servicio, $descripcion_servicio, $disponibilidad_servicio);
            }
        } else {
            echo "Faltan datos en los campos";
        }
    }

    if (isset($_POST["eliminar_servicio"])) {
        foreach ($_POST['check_servicio'] as $checked) {
            eliminar_servicios($checked);
        }
    }
    if (isset($_POST["modificar_servicio"])) {
        header("Location: Modificar_servicios.php?nombre=".$_POST['check_servicio']);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <title>Servicios</title>
</head>

<body>
    <h3>Nuevo Servicio</h3>
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_servicio" name="formulario_servicio">
        <label for="nombre_servicio">Nombre</label><br>
        <input type="text" id="nombre_servicio" name="nombre_servicio" required><br>
        <label for="precio_servicio">Precio</label><br>
        <input type="text" id="precio_servicio" name="precio_servicio" required><br>
        <label for="descripcion_servicio">Descripcion</label><br>
        <input type="text" id="descripcion_servicio" name="descripcion_servicio" required><br>
        <label for="disponibilidad_servicio">Disponibilidad</label><br>
        <input type="checkbox" id="disponibilidad_servicio" name="disponibilidad_servicio"><br>
        <input type="submit" id="nuevo_servicio" name="nuevo_servicio" value="Añadir">
    </form>
    <div>
        <h3>Lista de Servicios</h3>
        <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST">
            <?php
            $array_servicios = listar_servicios();
            foreach ($array_servicios as $servicio) {
                print_r("<input type='checkbox' id='check_servicio' name='check_servicio' value='" . $servicio["nombre_servicio"] . "'>" . $servicio["nombre_servicio"] . "</br>");
            }
            ?>
    </div>
    <input type="submit" id="eliminar_servicio" name="eliminar_servicio" value="Eliminar">
    <input type="submit" id="modificar_servicio" name="modificar_servicio" value="Modificar">
    </form>
    <br>
    <a href="./Index.php"  style="text-decoration: none; color:white;"> De vuelta a casa</a>
</body>

</html>