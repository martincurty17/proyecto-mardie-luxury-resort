<?php

require_once '../DB/gestionar_usuarios.php';
require_once '../DB/gestionar_reservas.php';
session_start();


if (isset($_POST["modificar"])) {

    $nombre = $_POST["nombre"];
    $email = $_POST["email"];
    $email2 = $_POST["email2"];
    $telefono = $_POST["telf"];
    $direccion = $_POST["direccion"];

    if ($email == $email2) {
        if ($_POST["password"] == $_POST["password2"]) {

            $password = password_hash($_POST["password"], PASSWORD_DEFAULT);

            modificar_perfil($nombre, $password, $email, $telefono, $direccion, $_SESSION["id_usuario"]);
        }
    }
}

if (isset($_POST["eliminar_reserva"])) {
    foreach ($_POST['check_reserva'] as $checked) {        
        eliminar_reserva($checked);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../styles/inserthab/estilos.css">
    <title>Administrar Perfil</title>
</head>

<body>
    <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_modificar" name="formulario_modificar">
        <label for="nombre">Nombre Usuario: </label><br>
        <input type="text" id="nombre" name="nombre" value="<?php echo $_SESSION["nombre"] ?>"><br>
        <label for="password">Cambiar Contraseña: </label><br>
        <input type="password" id="password" name="password"><br>
        <label for="password2">Repetir Contraseña: </label><br>
        <input type="password" id="password2" name="password2"><br>
        <label for="email">Cambiar Correo Electrónico: </label><br>
        <input type="text" id="email" name="email" value="<?php echo $_SESSION["email"] ?>"><br>
        <label for="email2">Repetir Correo Electrónico: </label><br>
        <input type="text" id="email2" name="email2"><br>
        <label for="telf">Telefono: </label><br>
        <input type="text" id="telf" name="telf" maxlength="9" value="<?php echo $_SESSION["telf"] ?>"><br>
        <label for="direccion">Direccion: </label><br>
        <input type="text" id="direccion" name="direccion" value="<?php echo $_SESSION["direccion"] ?>"><br>

        <input type="submit" id="modificar" name="modificar"><br>
        <h1>Lista de Reservas</h1>
            <?php
                if(isset($_SESSION['rol'])&&$_SESSION['rol']==2){
                    $array_reservas = listar_reservas_usuarios();
                    foreach ($array_reservas as $reservas) {
                        $nombre_usuario = buscar_usuario_id($reservas["id_usuario"]);
                        print_r("<input type='checkbox' id='check_reserva' name='check_reserva[".$reservas["num_reserva"]."]' value='".$reservas["num_reserva"]."'>Usuario: ".$nombre_usuario[0][0]."  Numero Reserva: ".$reservas["num_reserva"]."  Fecha de Entrada: ".$reservas["fecha_entrada"] . "  Fecha de salida: ".$reservas["fecha_salida"]."</br>");
                }
                }else{
                    $array_reservas = listar_reservas_usuario($_SESSION["id_usuario"]);
                    foreach ($array_reservas as $reservas) {
                    print_r("<input type='checkbox' id='check_reserva' name='check_reserva[".$reservas["num_reserva"]."]' value='".$reservas["num_reserva"]."'> Numero Reserva: ".$reservas["num_reserva"]." Fecha de Entrada: ".$reservas["fecha_entrada"] . " Fecha de salida: ".$reservas["fecha_salida"]."</br>");
                }
                }                
            ?>
        <br>
        <input type="submit" id="eliminar_reserva" name="eliminar_reserva" value="Eliminar Reserva"><br>
        <a href="./Index.php"  style="text-decoration: none; color:white;"> De vuelta a casa</a>
    </form>

    

</body>

</html>
