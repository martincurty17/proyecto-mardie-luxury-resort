<!DOCTYPE html>
<html lang="es">
<?php
require_once "../autoload.php";
include  "../DB/gestionar_usuarios.php";
include "../DB/gestionar_habitaciones.php";
include "../DB/gestionar_servicios.php";

require_once 'Envio_correo.php';


session_start();
$flag = FALSE;

/**
 * Usuarios Diego o Martin
 * Contraseña abc123. y 1234 respectivamente
 * session_start();
 */

$nombre = "";
$email = "";
$email2 = "";
$telefono = "";
$direccion = "";


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST["registrar"])) {
        /**
         * Asociamos las variables con los valores recibidos del formulario
         */
        $nombre = $_POST["nombre"];
        $email = $_POST["email"];
        $email2 = $_POST["email2"];
        $telefono = $_POST["telf"];
        $direccion = $_POST["direccion"];


        /**
         * Comprobamos que ningun campo del formulario este vacio
         */
        if (!empty($_POST["nombre"]) && !empty($_POST["password"]) && !empty($_POST["password2"]) && !empty($_POST["email"]) && !empty($_POST["email2"]) && !empty($_POST["telf"]) && !empty($_POST["direccion"])) {
            if ($email == $email2) {
                if ($_POST["password"] == $_POST["password2"]) {

                    $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
                    /**
                     * Metodo que se encarga de añadir el usuario a la base de datos
                     */
                    registrar_usuario($nombre, $password, $email, $telefono, $direccion);
                    enviar_correos($email, $nombre);
                    /**
                     * AQUI REDIRECCION A LA PAGINA PRINCIPAL CON RESERVAS
                     */
                    echo "Se ha registrado correctamente";
                } else {
                    echo "Las contraseñas no coinciden";
                }
            } else {
                echo "Los correos no coinciden";
            }
        } else {
            echo "faltan datos";
        }
    }

    if (isset($_POST["login"])) {
        $nombreLogin = $_POST["nombreLogin"];
        if (!empty($_POST["nombreLogin"]) && !empty($_POST["passwordLogin"])) {
            $passwordLogin = $_POST["passwordLogin"];
           $flag = login_usuario($nombreLogin, $passwordLogin);
            echo "Logueado Correctamente";
        } else {
            echo "Faltan datos del usuario";
        }
    }
}
if (isset($_POST["cerrar_sesion"])) {
    cerrar_sesion();
}
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../styles/estilos.css">

    <title>Mardie L.Resort</title>
</head>

<body>
    <div class="fondo">

        <header>
            <div id="sidebar" class="sidebar">
                <a href="#" class="boton-cerrar" onclick="ocultar()">×</a>
                <ul class="men_desp">
                    <?php
                    
                    if ($flag) {
                        echo "<li> Welcome " . $_POST["nombreLogin"] . "</li>
                    <form action='". htmlspecialchars($_SERVER["PHP_SELF"]) ."' method='POST' id='formulario_cerrar_sesion' name='formulario_cerrar_sesion'>
                    <input type='submit' id='cerrar_sesion' name='cerrar_sesion' value='Log Out'>
                </form>
                    ";
                         
                    } else {
                    ?>
                        <li><a onclick="iniciosesion()">Login</a> </li>
                        <li>
                            <div id="formulario">
                                <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_login" name="formulario_login">
                                    <label for="nombre">User: </label><br>
                                    <input type="text" id="nombreLogin" name="nombreLogin"><br>
                                    <label for="password">Password: </label><br>
                                    <input type="password" id="passwordLogin" name="passwordLogin"><br>
                                    <button type="submit" id="registrar" name="login">Log In</button>

                                </form>
                            </div>
                        </li>
                        <li><a onclick="registro()">Sing up</a></li>
                        <li>
                            <div id="formulario2">
                                <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST" id="formulario_registro" name="formulario_registro">
                                    <label for="nombre">Name: </label><br>
                                    <input type="text" id="nombre" name="nombre"><br>
                                    <label for="password">Password: </label><br>
                                    <input type="password" id="password" name="password"><br>
                                    <label for="password2">Repeat Password: </label><br>
                                    <input type="password" id="password2" name="password2"><br>
                                    <label for="email">Email: </label><br>
                                    <input type="text" id="email" name="email"><br>
                                    <label for="email2">Repeat Email: </label><br>
                                    <input type="text" id="email2" name="email2"><br>
                                    <label for="telf">Tlf: </label><br>
                                    <input type="text" id="telf" name="telf" maxlength="9"><br>
                                    <label for="direccion">Direction: </label><br>
                                    <input type="text" id="direccion" name="direccion"><br>

                                    <button type="submit" id="registrar" name="registrar">Sing Up</button>

                                </form>
                                </form>
                            </div>
                        </li>
                    <?php
                    }


                    ?>
                    <li><a href="../doc/index.html">About</a></li>
                    <li><a href="https://www.cookiebot.com/es/politica-de-privacidad-para-mi-web/?gclid=Cj0KCQjwnueFBhChARIsAPu3YkTMZzrXrBA4qP7yK0vDNO0I14PGeqiUG_eThzE7QbzSRAdYlWYlYZsaApTbEALw_wcB">Politics and Privacy</a></li>
                </ul>
            </div>
            <div class="despleg">
                <button class="rayitas" onclick="mostrar()"><canvas id="rayas" width="40" height="40">

                    </canvas></button>
            </div>
            <div class="titulo">
                <img class="uno" src="../img/Titulo2.png" alt="Mardie L.Resort" srcset="">
                <div class="dos">
                    <img src="../img/tortuga_icon2.png" alt="" srcset="">
                    <p> Mardie Luxury Resort</p>
                </div>
            </div>

            <div class="idioma">
                <p> <img src="../img/flag_uk_icon.png" alt="bandera inglaterra" srcset="">
                </p>
                <span> English</span>
            </div>



            <div class="#">
            </div>
            <div>
                <nav class="subnav">
                    <ul class="navigation">
                    <?php 
                    if(isset($_SESSION['rol'])&&$_SESSION['rol']==2){
                                     
                    ?>
                    <a href="./Servicios.php" style="text-decoration: none; color:white;"><li>Service Manage</li></a>
                    <a href="./Habitaciones.php" style="text-decoration: none; color:white;"><li>Manage Rooms</li></a>
            <?php
            }else{ 
                  echo "<li>Resort Overview</li>  
                  <li>Location</li>";
                 };                 
                    ?>
                        
                      
                        <li>Activities & Excursions</li>
                        <li>More...</li>
                    </ul>
                </nav>
            </div> 
            <?php 
                    if(isset($_SESSION['rol'])){
                                     
                    ?>
            <div class="checkButton">
                <form action="./Usuario.php" method="get">
                    <button id="hab" type="sumbit" value="">Options</button>
                </form>
            </div>
            <?php
                    }
            ?>
        </header>


        <div class="entrada">
            <p>Welcome To Paradise </p>

            <div class="estrellitas">
                <img src="../img/estrellita.png" width="40" height="40">
                <img src="../img/estrellita.png" width="40" height="40">
                <img src="../img/estrellita.png" width="40" height="40">
                <img src="../img/estrellita.png" width="40" height="40">
                <img src="../img/estrellita.png" width="40" height="40">
            </div>

        </div>

    </div>

    <div class="contenido">

        <?php
        $habitaciones = listar_tipos_habitaciones();
        foreach ($habitaciones as $hab) {
            $tipo = $hab['tipo_de_habitacion'];
            $m2 = $hab['m2'];
            if ($hab['ventana'] == 1) {
                $vent = "Si";
            } else {
                $vent = "No";
            };
            if ($hab['servicio_limpieza'] == 1) {
                $limp = "Si";
            } else {
                $limp = "No";
            };
            if ($hab['internet'] == 1) {
                $int = "Si";
            } else {
                $int = "No";
            }
            $precio = $hab['precio'];
            $descripcion = $hab['descripcion'];

           $img= visualizar_imagenes($hab['id']);
           $ruta=$img[0][2];
            echo '<img src="'.$ruta.'" alt="Habitacion de Hotel"> <div class= "texto"><p class="title">' . $tipo . '</p>
            <p>Metros Cuadrados: ' . $m2 . '</p><p> Ventana: ' . $vent . '</p><p> Servicio de Limpieza: ' . $limp . '</p><p>Internet: ' . $int . '</p><p>Precio: ' . $precio . '</p><p class="descripcion">Descrpcion: ' . $descripcion . '</p> ';
            if ($flag) { 

                echo  '<form action="./Reservas.php"  method="post">
                <input type="text" name="habitacion" value="'.$tipo.'" style="display:none">
                <button  type="submit">Rerserva</button></form>';
               
            }
            echo
            '
        </div>';
        }

        ?>


    </div>
    <div class="subrese">
        <button>Haz tu Reserva</button>
    </div>
    <footer class="submenu">
        <nav class="footnav">
            <ul>
                <li>b</li>
                <li>c</li>
                <li>3</li>
                <li>4</li>
            </ul>

        </nav>
    </footer>
</body>
<script src="../js/scrpt.js"></script>

</html>